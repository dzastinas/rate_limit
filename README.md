## SETUP

Rate limiter is implemented as request filter with:
Kotlin, Http4k and resilience4j libs

Config file is lactated in root directory *ratelimit.yml*

RateLimiter uses "Fixed window counter" algorithm where filter splits all nanoseconds from the start of epoch into cycles and keeps track of counter.

![](/image/44ca055-rate_limiter.png)


All possible URL are Get:
- `GET /api/v1/accounts/1/deactivate`
- `GET /api/v1/accounts/1`
- `GET /api/v2/logs`
- `GET /api/v3/users`


How to start. Launch main function from IDEA.

Simple test is in ResilienceFiltersTest