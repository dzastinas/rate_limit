package net.justinas

import io.github.resilience4j.ratelimiter.RateLimiter
import io.github.resilience4j.ratelimiter.RequestNotPermitted
import org.http4k.core.Filter
import org.http4k.core.Response
import org.http4k.core.Status.Companion.TOO_MANY_REQUESTS

object ResilienceFilters {


    /**
     * Provide simple Rate Limiter functionality.
     */
    object RateLimit {
        operator fun invoke(
            rateLimit: RateLimiter = RateLimiter.ofDefaults("RateLimit"),
            onError: () -> Response = { Response(TOO_MANY_REQUESTS.description("Too Many Requests and do not process the request")) }
        ) = Filter { next ->
            {
                try {
                    rateLimit.executeCallable { next(it) }
                } catch (e: RequestNotPermitted) {
                    onError()
                }
            }
        }
    }

}