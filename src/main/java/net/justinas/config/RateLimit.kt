package net.justinas.config

data class RateLimit(val pattern: String = "", val limit: Int = 0)
