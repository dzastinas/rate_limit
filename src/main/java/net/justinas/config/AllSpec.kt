package net.justinas.config

import com.uchuhimo.konf.ConfigSpec

object AllSpec : ConfigSpec() {
    val rateLimiting by required<List<RateLimit>>()
}