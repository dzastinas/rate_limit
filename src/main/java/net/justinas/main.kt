package net.justinas

import com.uchuhimo.konf.Config
import io.github.resilience4j.ratelimiter.RateLimiter
import io.github.resilience4j.ratelimiter.RateLimiterConfig
import net.justinas.config.AllSpec
import net.justinas.config.RateLimit
import org.http4k.core.*
import org.http4k.core.Method.GET
import org.http4k.core.Status.Companion.OK
import org.http4k.filter.DebuggingFilters.PrintRequestAndResponse
import org.http4k.routing.*
import org.http4k.server.Jetty
import org.http4k.server.asServer
import java.time.Duration

fun main() {
    val config = Config { addSpec(AllSpec) }.from.yaml.file("ratelimit.yml")
    val rateList = config[AllSpec.rateLimiting]

    val routesWithFilter =
        PrintRequestAndResponse().then(
            routes(
                createRoute("/api/v1/accounts/{account}/deactivate" bind GET, rateList,
                    { request: Request -> Response(OK).body("Deactivate, ${request.path("account")}!") }),
                createRoute("/api/v1/accounts/{account}" bind GET, rateList,
                    { request: Request -> Response(OK).body("Info, ${request.path("account")}!") }),
                createRoute(
                    "/api/v2/logs" bind GET,
                    rateList,
                    { request: Request -> Response(OK).body(request.toString()) }),
                createRoute("/api/v3/users" bind GET, rateList, { _: Request -> Response(OK).body("YES") })
            )
        )

    val jettyServer = routesWithFilter.asServer(Jetty(9000)).start()
}

private fun createRoute(
    method: PathMethod,
    rateList: List<RateLimit>,
    function: (Request) -> Response
): RoutingHttpHandler {
    return method to getRateLimit(method.path, rateList, function)
}

private fun getRateLimit(
    path: String,
    limit: List<RateLimit>,
    function: (Request) -> Response
): HttpHandler {
    val findLimit = findLimit(path, limit)
    if (findLimit != null) {
        val rateConfig = RateLimiterConfig.custom()
            .limitRefreshPeriod(Duration.ofMinutes(1))
            .limitForPeriod(findLimit.limit)
            .timeoutDuration(Duration.ofSeconds(1))
            .build()
        return ResilienceFilters.RateLimit(RateLimiter.of(findLimit.pattern, rateConfig)).then { function.invoke(it) }
    } else {
        return function
    }
}

fun findLimit(listPath: String, rateList: List<RateLimit>): RateLimit? {
    val splitRate = listPath.split("/")
    for (config in rateList) {
        val splitConfig = config.pattern.split("/")
        if (config.pattern.isNotBlank() && splitRate.size == splitConfig.size) {
            splitRate.zip(splitConfig).map { it.second == "*" || it.first == it.second }.find { it == false }
                ?: return config
        }
    }
    return null
}
