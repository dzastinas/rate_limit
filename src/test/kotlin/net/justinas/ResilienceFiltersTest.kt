package net.justinas

import com.natpryce.hamkrest.assertion.assertThat
import com.natpryce.hamkrest.equalTo
import io.github.resilience4j.ratelimiter.RateLimiter
import io.github.resilience4j.ratelimiter.RateLimiterConfig
import org.http4k.core.Method.GET
import org.http4k.core.Request
import org.http4k.core.Response
import org.http4k.core.Status.Companion.OK
import org.http4k.core.Status.Companion.TOO_MANY_REQUESTS
import org.http4k.core.then
import org.junit.jupiter.api.Test
import java.time.Duration


class ResilienceFiltersTest {


    @Test
    fun `rate limit filter`() {
        val config = RateLimiterConfig.custom()
            .limitRefreshPeriod(Duration.ofSeconds(1))
            .limitForPeriod(1)
            .timeoutDuration(Duration.ofMillis(10)).build()

        val rateLimits = ResilienceFilters.RateLimit(RateLimiter.of("ratelimiter", config)).then { Response(OK) }

        assertThat(rateLimits(Request(GET, "/")).status, equalTo(OK))
        assertThat(rateLimits(Request(GET, "/")).status, equalTo(TOO_MANY_REQUESTS))
    }

}